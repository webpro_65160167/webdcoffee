import { ref, watch, type Ref } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/RecieptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { useLoadingStore } from './loading'
import { useMessageStore } from './massage'
import orderService from '@/service/order'

export const useReceiptStore = defineStore('receipt', () => {
  const memberStore = useMemberStore()
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const receiptDialog = ref(false)
  const qrDialog = ref(false)
  const erDialog = ref(false)
  const errorCashDialog = ref(false)
  const receipt = ref<Receipt>()
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      totalNet: 0,
      memberDiscount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: '',
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!,
      memberId: 0
    }
  }
  const receiptItems = ref<ReceiptItem[]>([])
  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  // function incSL(item: ReceiptItem) {
  //   const increment = 25
  //   if (item.sweetLevel + increment > 126) {
  //     window.alert("Can't increase the sweetness value beyond 125.")
  //   } else {
  //     item.sweetLevel += increment
  //   }
  // }

  // function decSL(item: ReceiptItem) {
  //   const decrement = 25
  //   if (item.sweetLevel - decrement < -1) {
  //     window.alert("Can't decrease the sweetness value beyond 125.")
  //   } else {
  //     item.sweetLevel -= decrement
  //   }
  // }
  function calReceipt() {
    let total = 0
    for (const item of receiptItems.value) {
      total = total + item.price * item.unit
    }
    const cash = receipt.value!.receivedAmount
    receipt.value!.total = total
    const point = receipt.value!.memberDiscount
    if (memberStore.editedMember) {
      receipt.value!.totalNet = total - point
      receipt.value!.change = cash - receipt.value!.totalNet
    } else {
      receipt.value!.totalNet = total
      receipt.value!.change = cash - receipt.value!.totalNet
    }
  }

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    receiptItems.value.push(newReceiptItem);
  }
  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      deleteReceiptItem(selectedItem)
    }
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }

  const order = async () => {
    try{
      loadingStore.doLoad()
      await orderService.addOrder(receipt.value!, receiptItems.value)
      initReceipt()
      loadingStore.finish()
    } catch(e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  return {
    addReceiptItem,
    receiptItems,
    receipt,
    calReceipt,
    receiptDialog,
    incUnitOfReceiptItem,
    // incSL,
    // decSL,
    qrDialog,
    errorCashDialog,
    erDialog,
    order,
    decUnitOfReceiptItem,
    removeItem
  }
})
