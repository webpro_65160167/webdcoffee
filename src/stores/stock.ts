import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import StockService from '@/service/stock'
import type { Stock } from '@/types/Stock'

export const useStockStore = defineStore('stock', () => {
  const stocks = ref<Stock[]>([])
  const loadingStore = useLoadingStore()

  const initialStock: Stock & { files: File[] } = {
    name: '',
    priceUnit: 0,
    quantity: 0,
    value: 0,
    image: 'nostock.jpg',
    files: [],
    stock: false
  }

  const editedStock = ref<Stock & { files: File[] }>(JSON.parse(JSON.stringify(initialStock)))

  async function getStock(id: number) {
    try {
      loadingStore.doLoad()
      const res = await StockService.getStock(id)
      editedStock.value = res.data
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }

  async function saveStock() {
    try {
      loadingStore.doLoad()
      const stock = editedStock.value
      if (!stock.id) {
        const res = await StockService.addStock(stock)
      } else {
        const res = await StockService.updateStock(stock)
      }
      await getStocks()
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }

  async function getStocks() {
    try {
      loadingStore.doLoad()
      const res = await StockService.getStocks()
      stocks.value = res.data
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }

  async function deleteStock() {
    try {
      loadingStore.doLoad()
      const stock = editedStock.value
      const res = await StockService.delStock(stock)
      await getStocks()
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }
  function clearForm() {
    editedStock.value = JSON.parse(JSON.stringify(initialStock))
  }

  return {
    getStocks,
    getStock,
    saveStock,
    deleteStock,
    clearForm,
    stocks,
    editedStock
  }
})
