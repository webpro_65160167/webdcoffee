import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import branchService from '@/service/branch'
import type { Branch } from '@/types/Branch'
// import { useMessageStore } from './message'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  // const messageStore = useMessageStore()
  const branchs = ref<Branch[]>([])
  const initialBranch: Branch = {
    name: '',
    address: '',
    email: '',
    tel: '',
    openDate: new Date()
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    editedBranch.value = res.data
    loadingStore.finish()
  }
  //Data function

  async function getBranchs() {
    try {
      loadingStore.doLoad()
      const res = await branchService.getBranchs()
      branchs.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
      console.error(e)
    }
  }

  async function saveBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    try {
      if (!branch.id) {
        //Add new
        console.log('Post' + JSON.stringify(branch))
        const res = await branchService.addBranch(branch)
      } else {
        // Update
        console.log('Patch' + JSON.stringify(branch))
        const res = await branchService.updateBranch(branch)
      }
      await getBranchs()
      loadingStore.finish()
    } catch (e) {
      // messageStore.showMessage(e.message)
      loadingStore.finish()
      console.error(e)
    }
  }

  async function deleteBranch() {
    try {
      loadingStore.doLoad()
      const branch = editedBranch.value
      const res = await branchService.delBranch(branch)

      await getBranchs()
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }
  return { branchs, getBranch, getBranchs, saveBranch, deleteBranch, clearForm, editedBranch }
})
