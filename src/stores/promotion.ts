import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import promotionService from '@/service/promotion'
import type { Promotion } from '@/types/Promotion'
import { useMessageStore } from './massage'

export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const promotions = ref<Promotion[]>([])
  const initialPromotion: Promotion & { files: File[] }= {
    name: '',
    condition: '',
    discount: 0,
    image: 'noimage.png',
    files: []
  }
  const editedPromotion = ref<Promotion & { files: File[] }>(JSON.parse(JSON.stringify(initialPromotion)))

  async function getPromotion(id: number) {
    try{
      loadingStore.doLoad()
      const res = await promotionService.getPromotion(id)
      editedPromotion.value = res.data
      loadingStore.finish()
    } catch(e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function getPromotions() {
    loadingStore.doLoad()
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    loadingStore.finish()
  }
  async function savePromotion() {
    try {
      loadingStore.doLoad()
      const promotion = editedPromotion.value
      if (!promotion.id) {
        // Add new
        console.log('Post ' + JSON.stringify(promotion))
        const res = await promotionService.addPromotion(promotion)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(promotion))
        const res = await promotionService.updatePromotion(promotion)
      }

      await getPromotions()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deletePromotion() {
    loadingStore.doLoad()
    const promotion = editedPromotion.value
    const res = await promotionService.delPromotion(promotion)

    await getPromotions()
    loadingStore.finish()
  }

  function clearForm() {
    editedPromotion.value = JSON.parse(JSON.stringify(initialPromotion))
  }
  return { promotions, getPromotions, savePromotion, deletePromotion, editedPromotion, getPromotion, clearForm }
})
