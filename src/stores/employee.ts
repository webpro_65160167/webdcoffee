import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import EmployeeService from '@/service/employee'
import http from '@/service/http'
import { type User } from '@/types/Employee'
export const useEmployeeStore = defineStore('employee', () => {
    const employees = ref<User[]>([])
    const loadingStore = useLoadingStore()
    const initialEmployee: User = {
        id: -1,
        name: "",
        gender: 'male',
        tel: "",
        username: "",
        password: "",
        role: 'barista',
        bankAccount: '',

    }
    const editedEmp = ref<User>(JSON.parse(JSON.stringify(initialEmployee)))

    async function getEmployees() {
        loadingStore.doLoad()
        const res = await EmployeeService.getEmployees()
        employees.value = res.data
        loadingStore.finish()
    }
    async function getEmployee(id: number) {
        loadingStore.doLoad()
        const res = await EmployeeService.getEmployee(id)
        editedEmp.value = res.data
        loadingStore.finish()
    }
    async function saveEmployee() {
        loadingStore.doLoad()
        const employee = editedEmp.value
        //add new
        if (employee.id === -1) {
            editedEmp.value.id = employees.value.length + 1
            const res = await EmployeeService.addEmployee(employee)
        } //update
        else {
            console.log(JSON.stringify(employee))
            const res = await EmployeeService.updateEmployee(employee)
        }
        await getEmployees()
        loadingStore.finish()
    }
    async function deleteEmployee() {
        loadingStore.doLoad()
        const employee = editedEmp.value
        const res = await EmployeeService.delEmployee(employee)
        await getEmployees()
        loadingStore.finish()
    }
    function clearForm() {
        editedEmp.value = JSON.parse(JSON.stringify(initialEmployee))
    }
    return { getEmployee, getEmployees, saveEmployee, deleteEmployee, clearForm, employees, editedEmp }
})