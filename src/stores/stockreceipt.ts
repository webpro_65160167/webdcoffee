import type { StockReceipt } from "@/types/StockReceipt";
import type { StockReceiptItem } from "@/types/StockReceiptItem";
import { defineStore } from "pinia";
import { ref, watch } from "vue";
import StockReceiptService from '@/service/stockReceipt'
import { useLoadingStore } from "./loading";
import type { Stock } from "@/types/Stock";

export const useStockReceiptStore = defineStore('stockReceipt', () => {
    const loadingStore = useLoadingStore()
    const stockReceipts = ref<StockReceipt>()
    const stockReceiptItems = ref<StockReceiptItem[]>([])
    const stockReceiptsData = ref<StockReceipt[]>([])
    const initialStockReceiptItem: StockReceiptItem = {
        id: -1,
        name: "",
        price: 0,
        quantity: 0,
        value: 0,
        stockId: -1,
        stock: null!
    }
    const initialStockReceipt: StockReceipt = {
        id: 0,
        receiptDate: new Date(),
        totalPrice: 0,
        totalQuantity: 0,
        stockReceiptItem: []!
    }
    const editedStockReceiptItem = ref<StockReceiptItem>(JSON.parse(JSON.stringify(initialStockReceiptItem)))
    const editedStockReceipt = ref<StockReceipt>(JSON.parse(JSON.stringify(initialStockReceipt)))

    async function getStockReceipt(id: number) {
        try {
            loadingStore.doLoad()
            const res = await StockReceiptService.getStockReceipt(id)
            editedStockReceipt.value = res.data[0]
            loadingStore.finish()
        } catch (error) {
            loadingStore.finish()
            console.error(error)
        }
    }
    async function getStockReceipts() {
        try{
            loadingStore.doLoad()
            const res =await StockReceiptService.getStockReceipts()
            stockReceipts.value =res.data
            stockReceiptsData.value =res.data
            loadingStore.finish()
        }catch(error){
            loadingStore.finish()
            console.error(error)
        }
    }
    async function saveStockReceipt(){
        try{
            loadingStore.doLoad()
            await StockReceiptService.addStockReceipt(stockReceipts.value!,stockReceiptItems.value)
            getStockReceipts()
            loadingStore.finish()
        }catch(error){
            loadingStore.finish()
            console.error(error)
        }
    }
   watch(
    stockReceiptItems,()=>{
        calculate()
    },
    {deep :true}
   )
   const calculate = function () {
    stockReceipts.value!.totalQuantity = 0
    stockReceipts.value!.totalPrice = 0
    for (const item of stockReceiptItems.value) {
        stockReceipts.value!.totalPrice += item.value
      stockReceipts.value!.totalQuantity += item.quantity
    }
  }
  async function addStockReceiptItems(s: Stock){
    const newStockReceipt: StockReceiptItem={
        id:-1,                         
        name:s.name,
        price:editedStockReceiptItem.value.price,
        quantity:editedStockReceiptItem.value.quantity,
        value:editedStockReceiptItem.value.price * editedStockReceiptItem.value.quantity,
        stockId:s.id!,
        stock:s
    }
   stockReceiptItems.value.push(newStockReceipt)
   clearStockReceiptItems()
  }
  function clearStockReceiptItems(){
    editedStockReceiptItem.value.id=-1,
    editedStockReceiptItem.value.name= "",
    editedStockReceiptItem.value.price= 0,
    editedStockReceiptItem.value.quantity= 0,
    editedStockReceiptItem.value.value= 0
  }
  function clearStockReceipt(){
    stockReceiptItems.value=[]
    stockReceipts.value={
        id: -1,
        receiptDate: new Date(),
        totalPrice: 0,
        totalQuantity: 0
    }
  }
  function removeStockReceiptItem(stockReceiptItem: StockReceiptItem) {
    const index = stockReceiptItems.value.findIndex((item) => item == stockReceiptItem)
    stockReceiptItems.value.splice(index, 1)
}
    return{
        stockReceipts,
        stockReceiptItems,
        stockReceiptsData,
        editedStockReceipt,
        editedStockReceiptItem,
        clearStockReceipt,
        clearStockReceiptItems,
        addStockReceiptItems,
        saveStockReceipt,
        getStockReceipt,
        getStockReceipts,
        removeStockReceiptItem
    }
})