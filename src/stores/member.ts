import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import memberService from '@/service/member'
import type { Member } from '@/types/Member'
import { useMessageStore } from './massage'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const members = ref<Member[]>([])
  const initialMember: Member = {
    id: -1,
    name: '',
    tel: '',
    point: 0,
  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initialMember)))

  async function getMember(id: number) {
    try{
      loadingStore.doLoad()
      const res = await memberService.getMember(id)
      editedMember.value = res.data
      loadingStore.finish()
    } catch(e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  async function getMemberByTel(tel: string) {
    try{
      loadingStore.doLoad()
      const res = await memberService.getMemberbyTel(tel)
      editedMember.value = res.data
      loadingStore.finish()
    } catch(e: any) {
      loadingStore.finish()
      editedMember.value = {
        id: -1,
        name: 'Not Found',
        tel: '',
        point: 0,
      }
      messageStore.showMessage("Member Not Found")
    }
  }

  async function getMembers() {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }
  async function saveMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      if (member.id === -1) {
        // Add new
        editedMember.value.id = members.value.length + 1
        console.log('Post ' + JSON.stringify(member))
        const res = await memberService.addMember(member)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(member))
        const res = await memberService.updateMember(member)
      }

      await getMembers()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    const res = await memberService.delMember(member)
    await getMembers()
    loadingStore.finish()
  }

  function clearForm() {
    editedMember.value = JSON.parse(JSON.stringify(initialMember))
  }
  return { members, getMembers, saveMember, deleteMember, editedMember, getMember, clearForm, getMemberByTel }
})
