import type { OrderReceiptItem } from './OrderReceiptItem'

type status = 'delivered' | 'waiting for shipment'

type OrderReceipt = {
  id: number
  orderDate?: Date
  receiveDate?: Date
  totalQty: number
  totalPrice: number
  statusOrder: status[]
  orderReceiptItem?: OrderReceiptItem[]
}
export type { OrderReceipt, status }
