import type { StockReceiptItem } from "./StockReceiptItem";

type StockReceipt={
    id: number;
    receiptDate: Date;
    totalPrice: number;
    totalQuantity: number;
    stockReceiptItem?: StockReceiptItem[]
}
export{ type StockReceipt }