import type { Stock } from "./Stock";

type StockReceiptItem={
    id:number;
    name: string;
    price: number;
    quantity: number;
    value: number;
    stockId: number;
    stock?:Stock;
} 

export{ type StockReceiptItem }