type Promotion = {
  id?: number
  name: string
  condition: string
  discount: number
  image: string
}
function getImageUrl(promotion: Promotion) {
  return `http://localhost:3000/images/promotions/${promotion.image}`
}
export { type Promotion, getImageUrl }
