type Stock = {
  id?: number
  name: string
  priceUnit: number
  quantity: number
  value: number
  stock: boolean
  image: string
}
function getImageUrl(stock: Stock) {
  return `http://localhost:3000/images/stocks/${stock.id}`
}

export { type Stock,getImageUrl }

