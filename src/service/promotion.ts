import type { Promotion } from '@/types/Promotion'
import http from './http'

function addPromotion(promotion: Promotion & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('name', promotion.name)
  fromData.append('condition', promotion.condition)
  fromData.append('discount', promotion.discount.toString())
  if(promotion.files && promotion.files.length > 0) fromData.append('file', promotion.files[0])
  return http.post('/promotions', fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updatePromotion(promotion: Promotion & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('name', promotion.name)
  fromData.append('condition', promotion.condition)
  fromData.append('discount', promotion.discount.toString())
  if(promotion.files && promotion.files.length > 0) fromData.append('file', promotion.files[0])
  return http.post(`/promotions/${promotion.id}`, fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delPromotion(promotion: Promotion) {
  return http.delete(`/promotions/${promotion.id}`)
}

function getPromotion(id: number) {
  return http.get(`/promotions/${id}`)
}

function getPromotions() {
  return http.get('/promotions')
}

function getPromotionsByType(typeId: number) {
  return http.get('/promotions/type/'+ typeId)
}

export default { addPromotion, updatePromotion, delPromotion, getPromotion, getPromotions, getPromotionsByType }
