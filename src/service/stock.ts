import type { Stock } from '@/types/Stock'
import http from './http'

function addStock(stock: Stock & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('name', stock.name)
  fromData.append('priceUnit', stock.priceUnit.toString())
  fromData.append('quantity', stock.quantity.toString())
  fromData.append('value', stock.value.toString())
  if (stock.files && stock.files.length > 0) fromData.append('file', stock.files[0])
  return http.post('/stocks', fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateStock(stock: Stock & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('name', stock.name)
  fromData.append('price', stock.priceUnit.toString())
  fromData.append('quantity', stock.quantity.toString())
  fromData.append('value', stock.value.toString())
  if (stock.files && stock.files.length > 0) fromData.append('file', stock.files[0])

  return http.post(`/stocks/${stock.id}`, fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delStock(stock: Stock) {
  return http.delete(`/stocks/${stock.id}`)
}

function getStock(id: number) {
  return http.get(`/stocks/${id}`)
}
function getStocks() {
  return http.get('/stocks')
}

export default { addStock, updateStock, delStock, getStock, getStocks }
