import type { StockReceipt } from "@/types/StockReceipt";
import type { StockReceiptItem } from "@/types/StockReceiptItem";
import http from "./http";

type StockReceiptDto ={
    stockReceiptItems: {
        stockId: number;
        quantity: number;
      }[];
      receiptDate: string;
}
function getStockReceipt(id:number){
    return http.get(`stockReceipts/${id}`)
}
function getStockReceipts(){
    return http.get(`stockReceipts`)
}
function addStockReceipt(stockReceipt: StockReceipt, stockReceiptItem:StockReceiptItem[]){
    const stockReceiptDto:StockReceiptDto ={
        stockReceiptItems:[],
        receiptDate:""
    }
    stockReceiptDto.receiptDate=stockReceipt.receiptDate.toISOString();
    stockReceiptDto.stockReceiptItems = stockReceiptItem.map(item => ({
        stockId: item.stockId,
        quantity: item.quantity
    }))
    return http.post('stockReceipts',stockReceiptDto)
}
export default{ addStockReceipt,getStockReceipt,getStockReceipts}