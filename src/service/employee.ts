import type { User } from '@/types/Employee'
import http from './http'

function addEmployee(employee: User) {
    return http.post('/employees', employee)
}

function updateEmployee(employee: User) {
    return http.patch(`/employees/${employee.id}`, employee)
}

function delEmployee(employee: User) {
    return http.delete(`/employees/${employee.id}`)
}

function getEmployee(id: number) {
    return http.get(`/employees/${id}`)
}

function getEmployees() {
    return http.get('/employees')
}

export default { addEmployee, updateEmployee, delEmployee, getEmployee, getEmployees }
